﻿using UnityEngine;
using System.Collections;
using Photon;
using Custom.Utilities;

public class ConnectionManager : Singleton<ConnectionManager>
{
	const string VERSION = "v0.0.1";
	void Start()
	{
		if (PhotonNetwork.AuthValues == null)
			PhotonNetwork.AuthValues = new AuthenticationValues ();

		if (PlayerPrefs.HasKey ("userName"))
			PhotonNetwork.AuthValues.UserId = PlayerPrefs.GetString ("userName");
		else
			PhotonNetwork.AuthValues.UserId = "User_" + Random.Range(0, 999999999);

		print (PhotonNetwork.AuthValues.UserId);
		PhotonNetwork.ConnectUsingSettings (VERSION);

	}

	void Connect()
	{
		RoomOptions roomOptions = new RoomOptions (){ IsVisible = true, MaxPlayers = 6 };
		PhotonNetwork.JoinOrCreateRoom ("table_01", roomOptions, TypedLobby.Default);
	
	}

	void OnConnectedToMaster()
	{
		Connect ();
	}

	void OnJoinedRoom()
	{
		print ("joined the room_" + PhotonNetwork.room.Name + " with maxplayer " + PhotonNetwork.room.MaxPlayers);
	}

	[PunRPC]
	void OnJoiningUpdateData(PhotonPlayer player)
	{
		
	}
}

