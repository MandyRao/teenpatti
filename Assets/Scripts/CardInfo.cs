﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardInfo : MonoBehaviour
{

	public Sprite _cardImage;
	public int CardValue;
	public CardType _type;
	public bool _isFace;
	public FaceType _faceType;
}
