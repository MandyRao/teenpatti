﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Custom.Utilities;
using UnityEngine.UI;

public class PlayerManager : Singleton<PlayerManager>
{
	public int _chips, _gems;
	public Text _betText;
	public Button _incButton, _decButton;
	public bool _isAI;
	public bool _isblind = true;


	public void Start ()
	{
		_chips = 200000;
		_gems = 10;
//		_chipsText.text = _chips.ToString ();
//		_gemsText.text = _gems.ToString ();
		CardManager.Instance._buttons.GetChild (0).GetComponent <Button> ().onClick.AddListener (() => Pack ());
		CardManager.Instance._buttons.GetChild (1).GetComponent <Button> ().onClick.AddListener (() => Show ());
		CardManager.Instance._buttons.GetChild (2).GetComponent <Button> ().onClick.AddListener (() => {
			if (_isblind)
				Blind ();
			else
				Chaal ();
		});
	}

	public void Pack ()
	{
		GameManager._isPacked = true;
	
		string _myMoveStr = "" + PhotonNetwork.player.ID + "_" + MoveType.pack.ToString () + "_" + "0";
		GameManager.Instance.UpdatePlayerValues (_myMoveStr);
		GameManager.Instance.NextPlayer (PhotonNetwork.player.ID);
	}

	public void Blind ()
	{
		GameManager.Instance.bet = int.Parse (_betText.text);
		GameManager.Instance.stake += GameManager.Instance.bet;
		_chips -= GameManager.Instance.bet;
		GameManager.Instance._potAmt.text = GameManager.Instance.stake.ToString ();

		string _myMoveStr = "" + PhotonNetwork.player.ID + "_" + MoveType.blind.ToString () + "_" + GameManager.Instance.bet;

		GameManager.Instance.UpdatePlayerValues (_myMoveStr);
		GameManager.Instance.NextPlayer (PhotonNetwork.player.ID);
		GameManager.Instance.CheckPotAmt ();
	}


	public void Show ()
	{
		GameManager.Instance.stake += GameManager.Instance.bet;
		_chips -= GameManager.Instance.bet;
		GameManager.Instance._potAmt.text = GameManager.Instance.stake.ToString ();
		string _myMoveStr = "" + PhotonNetwork.player.ID + "_" + MoveType.show.ToString () + "_" + GameManager.Instance.bet;
		GameManager.Instance.UpdatePlayerValues (_myMoveStr);

		GameManager.Instance.CallResult ();
	}


	public void SeeCards ()
	{
		_isblind = false;

		GameManager.Instance.SeeCards ();
	}


	PhotonPlayer _targetPlayer;

	public void SideShow ()
	{
		for (int i = 0; i < GameManager.Instance._allPlayers.Count; i++) {
			if (GameManager.Instance._allPlayers [i].ID == PhotonNetwork.player.ID) {
				_targetPlayer = GameManager.Instance._allPlayers [i - 1];
			}
		}
		int[] id = new int[]{ PhotonNetwork.player.ID, _targetPlayer.ID };
		GameManager.Instance.SideShowCalled (id);
	}

	public void Chaal ()
	{
		GameManager.Instance.bet = int.Parse (_betText.text);
		GameManager.Instance.stake += GameManager.Instance.bet;
		_chips -= GameManager.Instance.bet;
		GameManager.Instance._potAmt.text = GameManager.Instance.stake.ToString ();
		string _myMoveStr = "" + PhotonNetwork.player.ID + "_" + MoveType.chaal.ToString () + "_" + GameManager.Instance.bet;
		GameManager.Instance.UpdatePlayerValues (_myMoveStr);
		GameManager.Instance.NextPlayer (GameManager.myPhotonId);

		GameManager.Instance.CheckPotAmt ();
	}

	public void SetBet ()
	{
		_betText.text = GameManager.Instance.bet.ToString ();
		_incButton.interactable = true;
		_decButton.interactable = false;
	}

	public void Increase ()
	{
		GameManager.Instance.bet *= 2;
		_betText.text = GameManager.Instance.bet.ToString ();
		_incButton.interactable = false;
		_decButton.interactable = true;
	}

	public void Decrease ()
	{

		GameManager.Instance.bet /= 2;
		_betText.text = GameManager.Instance.bet.ToString ();
		_incButton.interactable = true;
		_decButton.interactable = false;
	}


}
