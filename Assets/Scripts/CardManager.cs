﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Custom.Utilities;

public class CardManager : Singleton<CardManager>
{
    
	public GameObject _cardPrefab;
	public Sprite _cardBackground;
	public Cards[] _cards;
	public Transform _playerPosition, _playerInfoPosition;
	public Transform _startPosition;


	public List<Transform> _playerPositions = new List<Transform> ();
	public List<RectTransform> _infoPositions = new List<RectTransform> ();
	public RectTransform _buttons;


	public void GenerateCards ()
	{
		GameManager._gameStarted = true;
		StartCoroutine (IeGenerateCards ());
	}

	IEnumerator IeGenerateCards ()
	{
		string mycardString = "";
		int playerId = 0;
		foreach (var kvp in GameManager.Instance._allotedCards) {
			if (kvp.Key == PhotonNetwork.player.ID) {
				mycardString = kvp.Value;
				playerId = kvp.Key;
			}
		}
			
		mycardString.TrimEnd ('/');
		var mycardNames = mycardString.Split ('/');
		_playerInfoPosition.transform.GetChild (1).gameObject.SetActive (true);

		List<Cards> myCards = new List<Cards> ();

		for (int i = 0; i < mycardNames.Length; i++) {
			for (int j = 0; j < _cards.Length; j++) {
				if (_cards [j]._name == mycardNames [i]) {
					myCards.Add (_cards [j]);
				}
			}
		}

		for (int j = 0; j < 3; j++) {
			var obj = Instantiate (_cardPrefab, _startPosition) as GameObject;
			obj.transform.position = Vector3.zero;
			obj.GetComponent<SpriteRenderer> ().sprite = _cardBackground;
			obj.GetComponent<CardInfo> ().CardValue = myCards [j].CardValue;
			obj.GetComponent<CardInfo> ()._type = myCards [j]._type;
			obj.GetComponent<CardInfo> ()._isFace = myCards [j]._isFace;
			obj.GetComponent<CardInfo> ()._faceType = myCards [j]._faceType;
			obj.GetComponent<CardInfo> ()._cardImage = myCards [j]._sprite;

			obj.transform.SetParent (_playerPosition);
			obj.transform.localScale = Vector3.one;
			obj.GetComponent<SpriteRenderer> ().sortingOrder = j;
			for (float timer = 0; timer < 0.2f; timer += Time.deltaTime) {
				obj.transform.localPosition = Vector3.Lerp (obj.transform.localPosition, new Vector3 (j, 0, 0), 0.3f);
				yield return null;
			}
			obj.transform.localPosition = new Vector3 (j, 0, 0);
		}

//		GameManager.Instance.NextPlayer ();

	}


}


[Serializable]
public class Cards
{
	public string _name;
	public Sprite _sprite;
	public int CardValue;
	public CardType _type;
	public bool _isFace;
	public FaceType _faceType;
}



public enum CardType
{
	spade,
	diamond,
	heart,
	club
}

public enum FaceType
{
	king,
	Queen,
	Ace,
	Joker
}
