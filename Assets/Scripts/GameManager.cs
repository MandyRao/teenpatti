﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Custom.Utilities;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon;
using System;

public class GameManager : Singleton<GameManager>
{
	public static int playerWhoIsIt = 0;
	public static int myPhotonId = 0;
	private static PhotonView ScenePhotonView;

	public Dictionary<int,PhotonPlayer> _allPlayers = new Dictionary<int,PhotonPlayer> ();
	public Dictionary<int, string> _allotedCards = new Dictionary<int, string> ();


	public Dictionary<int, Transform> _playersPosition = new Dictionary<int, Transform> ();
	public Dictionary<int, RectTransform> _InfoPositions = new Dictionary<int, RectTransform> ();


	public Dictionary<int, PhotonPlayer> _currentSessionPlayer = new Dictionary<int, PhotonPlayer> ();

	public int bet, stake, _maxPotAmt;
	public Text _potAmt, _potAmtWarning, _userUpdates, _timerText;
	public GameObject _sideShowPopup, _sideShowCards, _exitPopup;


	public static bool _isPacked = false;
	public static bool _gameStarted = false;


	PhotonPlayer taggedplayer;


	public Sprite[] _pics;



	// Use this for initialization
	public void Start ()
	{
		ScenePhotonView = this.GetComponent<PhotonView> ();
		bet = 200;
		_maxPotAmt = 2000;

//		for (int i = 0; i < _pics.Length; i++) {
//			if (_pics [i].name == PlayerPrefs.GetString ("_profileName"))
//				CardManager.Instance._playerPosition.parent.GetComponent <SpriteRenderer> ().sprite = _pics [i];
//		}
	}


	#region ConnectionCallbacks

	public void OnJoinedRoom ()
	{
		// game logic: if this is the only player, we're "it"

		for (int i = 0; i < PhotonNetwork.otherPlayers.Length; i++) {
			playerWhoIsIt = PhotonNetwork.otherPlayers [i].ID;

			_allPlayers.Add (playerWhoIsIt, PhotonNetwork.otherPlayers [i]);
			_playersPosition.Add (playerWhoIsIt, CardManager.Instance._playerPositions [i]);
			int index = CardManager.Instance._playerPositions.IndexOf (_playersPosition [playerWhoIsIt]);
			_InfoPositions.Add (playerWhoIsIt, CardManager.Instance._infoPositions [index]);
	
			_InfoPositions [playerWhoIsIt].transform.GetChild (0).GetComponent<Text> ().text = PhotonNetwork.otherPlayers [i].UserId;
			_InfoPositions [playerWhoIsIt].gameObject.SetActive (true);
		}


		playerWhoIsIt = PhotonNetwork.player.ID;
		myPhotonId = playerWhoIsIt;
		_allPlayers.Add (playerWhoIsIt, PhotonNetwork.player);
			
		_playersPosition.Add (playerWhoIsIt, CardManager.Instance._playerPosition);
		_InfoPositions.Add (playerWhoIsIt, CardManager.Instance._playerInfoPosition.GetComponent <RectTransform> ());
	
		_InfoPositions [playerWhoIsIt].transform.GetChild (0).GetComponent<Text> ().text = PhotonNetwork.player.UserId;
		_InfoPositions [playerWhoIsIt].gameObject.SetActive (true);

		//ScenePhotonView.RPC("TaggedPlayer", PhotonTargets.MasterClient, PhotonNetwork.player.ID);
		Debug.Log ("playerWhoIsIt: " + playerWhoIsIt);
	}

	public void OnPhotonPlayerConnected (PhotonPlayer player)
	{
		Debug.Log ("OnPhotonPlayerConnected: " + player);

		// when new players join, we send "who's it" to let them know
		// only one player will do this: the "master"

		if (PhotonNetwork.isMasterClient) {
			TagPlayer (player);
		}
	}

	public static void TagPlayer (PhotonPlayer player)
	{
		Debug.Log ("TagPlayer: " + player.ID);
		ScenePhotonView.RPC ("TaggedPlayer", PhotonTargets.All, player.ID);
	}

	public void OnPhotonPlayerDisconnected (PhotonPlayer player)
	{
		Debug.Log ("OnPhotonPlayerDisconnected: " + player);
		playerWhoIsIt = player.ID;
		_allPlayers.Remove (playerWhoIsIt);
		_playersPosition.Remove (playerWhoIsIt);
		_InfoPositions [playerWhoIsIt].gameObject.SetActive (false);
		_InfoPositions.Remove (playerWhoIsIt);
	

		CheckPlayerDisconnection ();

	}

	#endregion

	#region Game Basic Functions

	public void UpdatePlayerValues (string move)
	{
		ScenePhotonView.RPC ("UpdatePlayerValueRPC", PhotonTargets.All, move);
	}

	public void SideShowCalled (int[] id)
	{
		ScenePhotonView.RPC ("SideShowRPC", PhotonTargets.All, id);
	}

	public void CheckPotAmt ()
	{
		if (stake >= _maxPotAmt) {
			//Game completed and now show result
//			ScenePhotonView.RPC ("PotAmtFullRPC", PhotonTargets.MasterClient, null);
		} else {
			return;
			//continue game
		}
	}

	public void CallResult ()
	{

	}

	public void DontShowCards (int[] id)
	{
		_sideShowPopup.SetActive (false);
		ScenePhotonView.RPC ("RejectShow", PhotonTargets.All, id);
	}

	public void ShowCards (int[] id)
	{
		_sideShowPopup.SetActive (false);
		ScenePhotonView.RPC ("AcceptShow", PhotonTargets.All, id);
	}



	void CheckPlayerWithNetwork ()
	{
		foreach (var player in PhotonNetwork.playerList) {
			if (!_allPlayers.ContainsKey (player.ID)) {
				_allPlayers.Add (player.ID, player);
			}
		}
	}

	void CheckPlayerDisconnection ()
	{
		Dictionary<int, PhotonPlayer> _playercheck = new Dictionary<int, PhotonPlayer> ();

		foreach (var player in PhotonNetwork.playerList) {
			if (!_playercheck.ContainsKey (player.ID)) {
				_playercheck.Add (player.ID, player);
			}
		}

		List<int> TobeRemovedId = new List<int> ();
		foreach (var kvp in _allPlayers) {
			if (!_playercheck.ContainsKey (kvp.Key)) {
				TobeRemovedId.Add (kvp.Key);
			}
		}

		foreach (var key in TobeRemovedId) {
			_allPlayers.Remove (key);
		}
	}

	public void SeeCards ()
	{
		var currentCardPos = _playersPosition [PhotonNetwork.player.ID];
		for (int i = 0; i < currentCardPos.childCount; i++) {
			currentCardPos.GetChild (i).GetComponent <SpriteRenderer> ().sprite = currentCardPos.GetChild (i).GetComponent <CardInfo> ()._cardImage;
			currentCardPos.GetChild (i).localScale = Vector3.one;
		}

		_InfoPositions [PhotonNetwork.player.ID].transform.GetChild (1).gameObject.SetActive (false);

	}


	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Escape))
			OnBack ();
	}



	public void OnBack ()
	{
		_exitPopup.SetActive (true);
		_exitPopup.transform.GetChild (1).GetComponent <Button> ().onClick.AddListener (() => {
			SceneManager.LoadScene (1);
		});
		_exitPopup.transform.GetChild (2).GetComponent <Button> ().onClick.AddListener (() => {
			_exitPopup.SetActive (false);
		});

	}


	#endregion


	#region RPC Functions

	[PunRPC]
	public void TaggedPlayer (int playerId)
	{
		foreach (var obj in PhotonNetwork.playerList) {
			if (obj.ID == playerId) {
				taggedplayer = obj;
			}
		}
		playerWhoIsIt = taggedplayer.ID;
		if (_allPlayers.ContainsKey (playerWhoIsIt)) {
			_allPlayers.Remove (playerWhoIsIt);
		} else {
			_allPlayers.Add (playerWhoIsIt, taggedplayer);
			_playersPosition.Add (playerWhoIsIt, CardManager.Instance._playerPositions [PhotonNetwork.otherPlayers.Length - 1]);
			int index = CardManager.Instance._playerPositions.IndexOf (_playersPosition [playerWhoIsIt]);
			_InfoPositions.Add (playerWhoIsIt, CardManager.Instance._infoPositions [index]);

			_InfoPositions [playerWhoIsIt].transform.GetChild (0).GetComponent<Text> ().text = taggedplayer.UserId;
			_InfoPositions [playerWhoIsIt].gameObject.SetActive (true);

		}

		if (!_gameStarted && PhotonNetwork.playerList.Length > 1) {
			print ("sending rpc of distribution");
			DistributeCards ();
		}
		Debug.Log ("TaggedPlayer: " + taggedplayer.ID);
	}


	[PunRPC]
	public void DistributeCards ()
	{
		StartCoroutine (IeDistributedCards ());
	}

	IEnumerator IeDistributedCards ()
	{
		for (float timer = 30f; timer >= 0f; timer -= Time.deltaTime) {
			_timerText.text = "Game Starts in " + (int)timer + " Secs";
			yield return null;
		}
		_timerText.gameObject.SetActive (false);
		foreach (var player in PhotonNetwork.playerList) {
			_currentSessionPlayer.Add (player.ID, player);
		}
		foreach (var pos in _currentSessionPlayer.Values) {
			string CardNames = "";
			for (int j = 0; j < 3; j++) {
				int index = UnityEngine.Random.Range (0, CardManager.Instance._cards.Length);
				CardNames += CardManager.Instance._cards [index]._name + "/";
			}
			if (!_allotedCards.ContainsKey (pos.ID))
				_allotedCards.Add (pos.ID, CardNames);
		}

		List<string> _sendString = new List<string> ();
		foreach (var kvp in _allotedCards) {
			string obj = kvp.Key + "_" + kvp.Value;
			_sendString.Add (obj);
		}
		var _sendobj = string.Join ("\n", _sendString.ToArray ());
		print ("sending rpc of updteCards");
		if (PhotonNetwork.isMasterClient)
			ScenePhotonView.RPC ("UpdateCards", PhotonTargets.All, _sendobj);
	}

	[PunRPC]
	public void UpdateCards (string _receivingString)
	{
		print (_receivingString);
		string[] _myDictionary = _receivingString.Split ('\n');
		for (int i = 0; i < _myDictionary.Length; i++) {
			var obj = _myDictionary [i].Split ('_');

			var id = int.Parse (obj [0].ToString ());
			var value = obj [1].ToString ();
			if (!_allotedCards.ContainsKey (id))
				_allotedCards.Add (id, value);
		}
		if (!_gameStarted)
			CardManager.Instance.GenerateCards ();
		if (PhotonNetwork.isMasterClient) {
			List<int> _allkeys = new List<int> ();
			foreach (var key in _allPlayers.Keys) {
				_allkeys.Add (key);
			}
			var index = 0;
			var targetid = 0;

			if (index < _allkeys.Count)
				targetid = _allkeys [index];
			else
				targetid = _allkeys [0];

			ScenePhotonView.RPC ("UpdateNextPlayer", PhotonTargets.All, _allPlayers [targetid]);
		}


	}

	[PunRPC]
	public void UpdatePlayerValueRPC (string movestr)
	{
		CheckPlayerWithNetwork ();



		Move move = new Move ();
		var splitstr = movestr.Split ('_');

		move.playerId = int.Parse (splitstr [0]);
		move._mType = (MoveType)(Enum.Parse (typeof(MoveType), splitstr [1]));
		move.Value = int.Parse (splitstr [2]);

		print (move.playerId);
		print (move.Value);

		foreach (var kvp in _currentSessionPlayer) {
			print (kvp.Key + " is " + kvp.Value.UserId);
		}
		//show updated values to all
		switch (move._mType) {

		case MoveType.blind:
			_userUpdates.text = "" + _currentSessionPlayer [move.playerId].UserId + " has played blind of " + move.Value;
			stake += (int)move.Value;
			bet = (int)move.Value;
//			if (PhotonNetwork.isMasterClient)
//				NextPlayer (move.playerId);

			break;
		case MoveType.chaal:
			_userUpdates.text = "" + _currentSessionPlayer [move.playerId].UserId + " has played chaal of " + move.Value;
			stake += (int)move.Value;
			if (move.Value / 2 > bet)
				bet = (int)(move.Value / 2);

//			if (PhotonNetwork.isMasterClient)
//				NextPlayer (move.playerId);
			break;
		case MoveType.pack:
			_userUpdates.text = "" + _currentSessionPlayer [move.playerId].UserId + " has packed";

//			if (PhotonNetwork.isMasterClient)
//				NextPlayer (move.playerId);
			break;
		case MoveType.show:
			_userUpdates.text = "" + _currentSessionPlayer [move.playerId].UserId + " has called Show";

			var currentCardPos = _playersPosition [PhotonNetwork.player.ID];
			for (int i = 0; i < currentCardPos.childCount; i++) {
				currentCardPos.GetChild (i).GetComponent <SpriteRenderer> ().sprite = currentCardPos.GetChild (i).GetComponent <CardInfo> ()._cardImage;
				currentCardPos.GetChild (i).localScale = Vector3.one;
			}
			ResultDeclared ();

			break;
		case MoveType.none:
		default:
			break;
		}


		if (stake >= _maxPotAmt) {
			ResultDeclared ();
			_potAmtWarning.gameObject.SetActive (true);
			return;
		}

	}


	[PunRPC]
	public void NextPlayer (int id)
	{
		CardManager.Instance._buttons.gameObject.SetActive (false);
		List<int> _allkeys = new List<int> ();
		foreach (var key in _currentSessionPlayer.Keys) {
			_allkeys.Add (key);
		}
		var index = _allkeys.IndexOf (id);
		var targetid = 0;
		index++;
		if (index < _allkeys.Count)
			targetid = _allkeys [index];
		else
			targetid = _allkeys [0];
		
		ScenePhotonView.RPC ("UpdateNextPlayer", PhotonTargets.All, _allPlayers [targetid]);
	}

	[PunRPC]
	public void UpdateNextPlayer (PhotonPlayer player)
	{
		if (player.ID != PhotonNetwork.player.ID)
			return;
		
		if (_isPacked) {
			ScenePhotonView.RPC ("NextPlayer", PhotonTargets.MasterClient, PhotonNetwork.player.ID);
		} else {
			if (player.ID == PhotonNetwork.player.ID)
				CardManager.Instance._buttons.gameObject.SetActive (true);
		}


		if (_potAmtWarning.IsActive ()) {
			CardManager.Instance._buttons.gameObject.SetActive (false);
		}
	}

	[PunRPC]
	public void SideShowRPC (int[] id)
	{
		if (PhotonNetwork.player.ID == id [1]) {
			//show popup of sideshow request
			_sideShowPopup.SetActive (true);
			_sideShowPopup.transform.GetChild (0).GetComponent <Text> ().text = _allPlayers [id [0]].UserId + " has requested for a sideshow!!!";
			_sideShowPopup.transform.GetChild (1).GetComponent <Button> ().onClick.AddListener (() => ShowCards (id));
			_sideShowPopup.transform.GetChild (2).GetComponent <Button> ().onClick.AddListener (() => DontShowCards (id));
		}
	}


	[PunRPC]
	public void PotAmtFullRPC ()
	{
		if (stake >= _maxPotAmt) {
			ResultDeclared ();
			_potAmtWarning.gameObject.SetActive (true);
			return;
		}

	}

	[PunRPC]
	public void ResultDeclared ()
	{
		/*
		 	1. Trail or Set (three of same rank),
			2. Pure sequence,
			3. Sequence (or run),
			4. Color,
			5. Pair (two cards of same rank), and
			6. High Card. 

 			It should be noted that in a sequence A-K-Q is the highest ranked sequence while A-2-3 is the second highest ranked sequence.
		 */



		Dictionary<int, int> _idwrtRank = new Dictionary<int, int> ();
		Dictionary<int, int> _idwrtHigh = new Dictionary<int, int> ();


		foreach (var kvp in _allotedCards) {
			string mycardString = kvp.Value;

			mycardString.TrimEnd ('/');
			var mycardNames = mycardString.Split ('/');

			List<Cards> myCards = new List<Cards> ();

			for (int i = 0; i < mycardNames.Length; i++) {
				for (int j = 0; j < CardManager.Instance._cards.Length; j++) {
					if (CardManager.Instance._cards [j]._name == mycardNames [i]) {
						myCards.Add (CardManager.Instance._cards [j]);
					}
				}
			}
			if (myCards [0].CardValue == myCards [1].CardValue && myCards [0].CardValue == myCards [2].CardValue) {
				//trial
				_idwrtRank.Add (kvp.Key, 1);
			} else if (myCards [0]._type == myCards [1]._type && myCards [0]._type == myCards [2]._type) {
				//pure sequence or color
				if ((myCards [0].CardValue == myCards [1].CardValue - 1 && myCards [0].CardValue == myCards [2].CardValue - 2) || (myCards [0].CardValue == myCards [1].CardValue + 1 && myCards [0].CardValue == myCards [2].CardValue + 2)) {
					// pure sequence
					_idwrtRank.Add (kvp.Key, 2);

				} else {
					//color
					_idwrtRank.Add (kvp.Key, 3);
				}
			} else if ((myCards [0].CardValue == myCards [1].CardValue - 1 && myCards [0].CardValue == myCards [2].CardValue - 2) || (myCards [0].CardValue == myCards [1].CardValue + 1 && myCards [0].CardValue == myCards [2].CardValue + 2)) {
				//sequence
				_idwrtRank.Add (kvp.Key, 4);
			} else if ((myCards [0].CardValue == myCards [1].CardValue && myCards [0].CardValue != myCards [2].CardValue) ||
			           (myCards [0].CardValue != myCards [1].CardValue && myCards [0].CardValue == myCards [2].CardValue) ||
			           (myCards [0].CardValue != myCards [1].CardValue && myCards [1].CardValue == myCards [2].CardValue)) {
				//pair
				_idwrtRank.Add (kvp.Key, 5);

			} else {
				int highestCardValue = myCards [0].CardValue;

				for (int i = 0; i < myCards.Count; i++) {
					if (myCards [i].CardValue > highestCardValue)
						highestCardValue = myCards [i].CardValue;
				}
				_idwrtHigh.Add (kvp.Key, highestCardValue);
			}
		}
		_potAmtWarning.gameObject.SetActive (false);

		List<int> winnerId = new List<int> ();
		int winnerRank = 5;
		if (_idwrtRank.Count > 0) {
			foreach (var kvp in _idwrtRank) {
				if (kvp.Value <= winnerRank) {
					if (kvp.Value < winnerRank)
						winnerId.Clear ();
					winnerId.Add (kvp.Key);
					winnerRank = kvp.Value;
				}
			}
		}



		ScenePhotonView.RPC ("ShowResult", PhotonTargets.All, winnerId.ToArray ());
	}

	[PunRPC]
	public void ShowResult (int[] winnerid)
	{
		
		_potAmtWarning.gameObject.SetActive (true);
		_potAmtWarning.text = "" + winnerid [0] + "has win";
	}


	[PunRPC]
	void RejectShow (int[] id)
	{
		if (id [0] == PhotonNetwork.player.ID) {
			_userUpdates.text = "" + _allPlayers [id [1]].UserId + " has rejected your request!!!";
		}
	}

	[PunRPC]
	void AcceptShow (int[] id)
	{
		if (id [0] == PhotonNetwork.player.ID) {
			string mycardString = _allotedCards [id [1]];

			mycardString.TrimEnd ('/');
			var mycardNames = mycardString.Split ('/');

			List<Cards> myCards = new List<Cards> ();

			for (int i = 0; i < mycardNames.Length; i++) {
				for (int j = 0; j < CardManager.Instance._cards.Length; j++) {
					if (CardManager.Instance._cards [j]._name == mycardNames [i]) {
						myCards.Add (CardManager.Instance._cards [j]);
					}
				}
			}

			_sideShowCards.SetActive (true);
			for (int i = 0; i < myCards.Count; i++) {
				_sideShowCards.transform.GetChild (i).GetComponent <Image> ().sprite = myCards [i]._sprite;
			}
			_sideShowCards.transform.GetChild (4).GetComponent <Button> ().onClick.AddListener (() => {
				_sideShowCards.SetActive (false);
			});
		}
	}



	#endregion
}

public class Move
{
	public int playerId;
	public MoveType _mType;
	public float Value;
}

public enum MoveType
{
	none = 0,
	chaal = 1,
	blind = 2,
	pack = 3,
	show = 4,
	sideshow = 5
}
