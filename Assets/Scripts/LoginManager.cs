﻿using Custom.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class LoginManager : Singleton<LoginManager>
{


	public InputField _emailid, _password, _SignUpEmailId, _signupUserName, _signUpPwd;
	public GameObject _loginPanel, _signUpPanel, _menuPanel, _tablePanel, _introPopup, _profilePanel, _exitPopup;


	public string _email, _username;



	bool _firstTimeLogin;

	public void Start ()
	{
		PlayerPrefs.SetString ("_profileName", "dummy-profile-pic");
		PlayerPrefs.SetInt ("played", 1);
		if (!PlayerPrefs.HasKey ("played")) {
			_loginPanel.SetActive (false);
			_signUpPanel.SetActive (true);
			_menuPanel.SetActive (false);
			_tablePanel.SetActive (false);
			_profilePanel.SetActive (false);
		} else {
			_loginPanel.SetActive (false);
			_signUpPanel.SetActive (false);
			_menuPanel.SetActive (true);
			_tablePanel.SetActive (false);
			_profilePanel.SetActive (false);
		}
	}


	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Escape))
			OnBack ();
	}


	public Sprite[] _pics;
	public GameObject _picPrefab, _picView, _picparent;

	public Image _profilePicinProfile, _profilePicinMenu;
	public string _profilepicName;
	public Text _uName, _pLevel, _pGame, _pwon, _ploss;

	public void OnProfilePicUpdate ()
	{
		_uName.text = _username;
		_pLevel.text = "0";
		_pGame.text = "0";
		_pwon.text = "0";
		_ploss.text = "0";
		_picView.SetActive (true);
		for (int i = 0; i < _pics.Length; i++) {
			var obj = Instantiate (_picPrefab, _picparent.transform);
			obj.GetComponent <Image> ().sprite = _pics [i];
			obj.GetComponent <Button> ().onClick.AddListener (() => {
				_profilePicinMenu.sprite = _pics [i];
				_profilePicinProfile.sprite = _pics [i];
				PlayerPrefs.SetString ("_profileName", _pics [i].name);
				_picView.SetActive (false);
			});
		}
	}

	public void OnLoginPanelClick ()
	{
		_loginPanel.SetActive (true);
		_signUpPanel.SetActive (false);
		_menuPanel.SetActive (false);
		_tablePanel.SetActive (false);

	}

	public void OnSignUp ()
	{
		DataManager.Instance.OnsignUpClick ();
	}

	public void OnSignupsuccess ()
	{
		_loginPanel.SetActive (false);
		_signUpPanel.SetActive (false);
		_menuPanel.SetActive (true);
		_tablePanel.SetActive (false);
		_firstTimeLogin = false;

	}

	public void OnLogin ()
	{
		DataManager.Instance.OnLoginClick ();
	}

	public void OnLoginSuccess ()
	{
		_loginPanel.SetActive (false);
		_signUpPanel.SetActive (false);
		_menuPanel.SetActive (true);
		_tablePanel.SetActive (false);
		_firstTimeLogin = false;

	}

	public void OnForgotPassword ()
	{
		Application.OpenURL ("www.google.co.in");
	}


	#region MenuButtons

	public void OnPlayNow ()
	{
		_loginPanel.SetActive (false);
		_signUpPanel.SetActive (false);
		_menuPanel.SetActive (false);
		_tablePanel.SetActive (true);
	}


	public void ShowPopup ()
	{
		_introPopup.SetActive (true);
	}

	public void OnAddAmount ()
	{
		
	}

	public void OnTutorial ()
	{
		
	}

	public void OnRules ()
	{
		
	}


	public void OnProfileView ()
	{
		
	}

	public void OnBack ()
	{
		if (_loginPanel.activeInHierarchy) {
			_loginPanel.SetActive (false);
			_signUpPanel.SetActive (true);
			_menuPanel.SetActive (false);
			_tablePanel.SetActive (false);
			_profilePanel.SetActive (false);

		} else if (_tablePanel.activeInHierarchy || _profilePanel.activeInHierarchy) {
			_loginPanel.SetActive (false);
			_signUpPanel.SetActive (false);
			_menuPanel.SetActive (true);
			_tablePanel.SetActive (false);
			_profilePanel.SetActive (false);

		} else if (_menuPanel.activeInHierarchy || _signUpPanel.activeInHierarchy) {
			_exitPopup.SetActive (true);
			_exitPopup.transform.GetChild (1).GetComponent <Button> ().onClick.AddListener (() => {
				Application.Quit ();
			});
			_exitPopup.transform.GetChild (2).GetComponent <Button> ().onClick.AddListener (() => {
				_exitPopup.SetActive (false);
			});

		}
	}


	public void OnPlayJoker ()
	{
		OnPlayNow ();
	}

	public void OnTournament ()
	{
		OnPlayNow ();

	}

	public void OnDeluxeTable ()
	{
		OnPlayNow ();

	}

	public void OnPrivateTable ()
	{
		OnPlayNow ();

	}

	#endregion

	#region TableSelection

	public Text _tableInfo;
	public TableInfo _selectedTable;
	public Button _startButton;

	public void SelectTable ()
	{
		_startButton.interactable = true;
		var _event = GameObject.FindObjectOfType<EventSystem> ();
		_selectedTable = _event.currentSelectedGameObject.GetComponent <TableInfo> ();
		_tableInfo.text = "Table Name: " + _selectedTable.TableName + "\nMinimum Pot Amount: " + _selectedTable.minBotAmt + "\nMaximum Stake Value: " + _selectedTable.maxStakeAmt;
		PlayerPrefs.SetInt ("Betamt", _selectedTable.minBotAmt);
		PlayerPrefs.SetInt ("stakeamt", _selectedTable.maxStakeAmt);
		PlayerPrefs.SetString ("table", _selectedTable.TableName);

	}

	public void StartGame ()
	{
		SceneManager.LoadScene (2);
	}

	#endregion
}
