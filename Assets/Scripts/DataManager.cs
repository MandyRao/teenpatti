﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using SimpleJSON;
using Custom.Utilities;

public class DataManager : Singleton<DataManager>
{

	public string __serverLink, __loginExt, __signupExt, __forgotPassword, __setProfilePic, __getPlayerData, __setPlayerData;

	string _loginToken;
	public int playerId;

	// Use this for initialization
	void Awake ()
	{
		DontDestroyOnLoad (this.gameObject);
		Invoke ("LoadNextScene", 1f);
	}

	void LoadNextScene ()
	{
		SceneManager.LoadScene (1);
	}

	//	public void SetPlayerData ()
	//	{
	//
	//	}
	//
	//	public IEnumerator IeSetPlayerData ()
	//	{
	//
	//	}
	//
	//	public void GetPlayerData ()
	//	{
	//
	//	}
	//
	//	public IEnumerator IeGetPlayerData ()
	//	{
	//
	//	}
	//
	//	public void OnSetProfilePic ()
	//	{
	//
	//	}
	//
	//	public IEnumerator IeOnSetProfilePic ()
	//	{
	//
	//	}
	//
	//	public void OnForgotPassword ()
	//	{
	//
	//	}
	//
	//	public IEnumerator IeOnForgotPassword ()
	//	{
	//
	//	}
	//
	//
	public void OnLoginClick ()
	{
		StartCoroutine (IeOnLoginClick ());
	}

	IEnumerator IeOnLoginClick ()
	{
		var link = string.Concat (__serverLink, __loginExt);

		LoginSendData data = new LoginSendData (LoginManager.Instance._emailid.text, LoginManager.Instance._password.text);

		var form = new Dictionary<string, string> ();
		form.Add ("content-type", "application/json");
		WWW www = new WWW (link, JsonUtility.FromJson<byte[]> (JsonUtility.ToJson (data)), form);

		yield return www;

		if (www.error == null) {
			var ReceivedData = JSON.Parse (www.text);
			if (ReceivedData ["success"] == "true") {
				_loginToken = ReceivedData ["loginToken"].ToString ().Trim ('"');
				int.TryParse (ReceivedData ["playerId"].ToString ().Trim ('"'), out playerId);
				print (ReceivedData ["message"]);

				LoginManager.Instance.OnLoginSuccess ();

			} else {
				print (ReceivedData ["message"]);
			}

		}
	}

	public void OnsignUpClick ()
	{
		StartCoroutine (IeOnSignUpClick ());
	}

	IEnumerator IeOnSignUpClick ()
	{
		var link = string.Concat (__serverLink, __signupExt);

		SignUp data = new SignUp (LoginManager.Instance._SignUpEmailId.text, LoginManager.Instance._signupUserName.text, LoginManager.Instance._signUpPwd.text);


		var form = new Dictionary<string, string> ();
		form.Add ("content-type", "application/json");


		WWW www = new WWW (link, JsonUtility.FromJson<byte[]> (JsonUtility.ToJson (data)), form);

		yield return www;

		if (www.error == null) {
			var ReceivedData = JSON.Parse (www.text);
			if (ReceivedData ["success"] == "true") {
				_loginToken = ReceivedData ["loginToken"].ToString ().Trim ('"');
				int.TryParse (ReceivedData ["playerId"].ToString ().Trim ('"'), out playerId);
				print (ReceivedData ["message"]);

				LoginManager.Instance.OnSignupsuccess ();

			} else {
				print (ReceivedData ["message"]);
			}

		}
	}

}

public class LoginSendData
{
	public string userName, password;

	public LoginSendData (string _usrName, string _password)
	{
		userName = _usrName;
		password = _password;
	}
}

public class SignUp
{

	public string email, userName, password;

	public SignUp (string _email, string _userName, string _pswd)
	{
		email = _email;
		userName = _userName;
		password = _pswd;
	}

}

public class ForgotPassword
{

	public string emailId, newPassword, confirmPassword;

	public ForgotPassword (string _email, string _pswd, string _cnfPwd)
	{
		emailId = _email;
		newPassword = _pswd;
		confirmPassword = _cnfPwd;
	}

}